module.exports = {
    output: {
        publicPath: '/js/',
        filename: '[name].js',
    },
    devtool: "source-map",
    module:  {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                loader: "babel-loader",
            },
            {
                test: /\.css$/,
                use: [ 'style-loader', 'css-loader' ]
            }
        ]
    }
};