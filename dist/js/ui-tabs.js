"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _merge = require("lodash/merge");

var _merge2 = _interopRequireDefault(_merge);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

var Tabs = function () {
    function Tabs(el, options) {
        _classCallCheck(this, Tabs);

        _initialiseProps.call(this);

        (0, _merge2.default)(this.options, options);

        var addEventOnTabs = this.addEventOnTabs,
            getTabpanelNameFromTab = this.getTabpanelNameFromTab,
            getTabpanelNameFromTabpanel = this.getTabpanelNameFromTabpanel,
            getTabsByTabpanelName = this.getTabsByTabpanelName,
            getTabpanelsByTabpanelName = this.getTabpanelsByTabpanelName,
            activateTabs = this.activateTabs,
            activateTabpanels = this.activateTabpanels,
            _options = this.options,
            _options$selectors = _options.selectors,
            parentSelector = _options$selectors.parent,
            tabSelector = _options$selectors.tab,
            wrapperSelector = _options$selectors.wrapper,
            tabpanelSelector = _options$selectors.tabpanel,
            _options$classNames = _options.classNames,
            activeTabClassName = _options$classNames.activeTab,
            activeTabpanelClassName = _options$classNames.activeTabpanel,
            _options$nameAttribut = _options.nameAttribute,
            tabNameAttribute = _options$nameAttribut.tab,
            tabpanelNameAttribute = _options$nameAttribut.tabpanel;


        var parent = this.nodes.parent = el;
        var tabs = this.nodes.tabs;
        var wrappers = this.nodes.wrappers;
        var tabpanels = this.nodes.tabpanels;

        parent.querySelectorAll(tabSelector).forEach(function (tab) {
            return tab.closest(parentSelector) === parent && tabs.push(tab);
        });
        parent.querySelectorAll(wrapperSelector).forEach(function (wrapper) {
            wrapper.closest(parentSelector) === parent && wrappers.push(wrapper);
            wrapper.querySelectorAll(tabpanelSelector).forEach(function (tabpanel) {
                return tabpanel.closest(wrapperSelector) === wrapper && tabpanels.push(tabpanel);
            });
        });

        addEventOnTabs();

        var activeTabs = this.active.tabs = tabs.filter(function (tab) {
            return tab.matches('.' + activeTabClassName);
        });
        var activeTabpanels = this.active.tabpanels = tabpanels.filter(function (tabpanel) {
            return tabpanel.matches('.' + activeTabpanelClassName);
        });

        if (!activeTabs.length && !activeTabpanels.length) {
            var activeTabpanelName = getTabpanelNameFromTab(tabs[0]);

            var newActiveTabs = this.active.tabs = getTabsByTabpanelName(activeTabpanelName);
            activateTabs(newActiveTabs);

            var newActiveTabpanels = this.active.tabpanels = getTabpanelsByTabpanelName(activeTabpanelName);
            activateTabpanels(newActiveTabpanels);
        } else if (activeTabs.length) {
            var _activeTabpanelName = getTabpanelNameFromTabpanel(activeTabs[0]);

            if (!activeTabs.every(function (activeTab) {
                return activeTab.getAttribute(tabNameAttribute) === _activeTabpanelName;
            })) {
                tabs.forEach(function (tab, i) {
                    if (i < 1) return;

                    if (tab.getAttribute(tabNameAttribute) === _activeTabpanelName && !tab.classList.contains(activeTabClassName)) {
                        tab.classList.add(activeTabClassName);
                    } else if (tab.classList.contains(activeTabClassName) && tab.getAttribute(tabNameAttribute) !== _activeTabpanelName) {
                        tab.classList.remove(activeTabClassName);
                    }
                });
            }

            var _newActiveTabpanels = this.active.tabpanels = getTabpanelsByTabpanelName(_activeTabpanelName);
            activateTabpanels(_newActiveTabpanels);
        } else if (activeTabpanels.length) {
            var _activeTabpanelName2 = getTabpanelNameFromTabpanel(activeTabpanels[0]);

            if (!activeTabpanels.every(function (activeTabpanel) {
                return activeTabpanel.getAttribute(tabpanelNameAttribute) === _activeTabpanelName2;
            })) {
                tabpanels.forEach(function (tabpanel, i) {
                    if (i < 1) return;

                    if (tabpanel.getAttribute(tabpanelNameAttribute) === _activeTabpanelName2 && !tabpanel.classList.contains(activeTabpanelClassName)) {
                        tabpanel.classList.add(activeTabpanelClassName);
                    } else if (tabpanel.classList.contains(activeTabpanelClassName) && tabpanel.getAttribute(tabpanelNameAttribute) !== _activeTabpanelName2) {
                        tabpanel.classList.remove(activeTabpanelClassName);
                    }
                });
            }

            var _newActiveTabs = this.active.tab = getTabsByTabpanelName(_activeTabpanelName2);
            activateTabs(_newActiveTabs);
        }
    }

    _createClass(Tabs, null, [{
        key: "getHeight",
        value: function getHeight(el) {
            return el.offsetHeight;
        }
    }]);

    return Tabs;
}();

var _initialiseProps = function _initialiseProps() {
    var _this = this;

    this.options = {
        selectors: {
            parent: ".ui-tabs",
            tab: ".ui-tabs__tab",
            wrapper: ".ui-tabs__wrapper",
            tabpanel: ".ui-tabs__tabpanel"
        },
        classNames: {
            wrapperHeightTransition: "ui-tabs__wrapper_height-transition",
            activeTab: "ui-tabs__tab_active",
            activeTabpanel: "ui-tabs__tabpanel_active"
        },
        nameAttribute: {
            tab: "data-ui-tabs-tabpanel",
            tabpanel: "data-ui-tabs-tabpanel"
        },
        events: {}
    };
    this.nodes = {
        tabs: [],
        wrappers: [],
        tabpanels: []
    };
    this.active = {};

    this.onTabClick = function (e) {
        e.preventDefault();

        var getTabpanelNameFromTab = _this.getTabpanelNameFromTab,
            activate = _this.activate;


        activate(getTabpanelNameFromTab(e.currentTarget));
    };

    this.addEventOnTabs = function () {
        var onTabClick = _this.onTabClick,
            tabs = _this.nodes.tabs;


        tabs.forEach(function (tab) {
            return tab.addEventListener("click", onTabClick);
        });
    };

    this.removeEventOnTabs = function () {
        var onTabClick = _this.onTabClick,
            tabs = _this.nodes.tabs;


        tabs.forEach(function (tab) {
            return tab.removeEventListener("click", onTabClick);
        });
    };

    this.getTabpanelNameFromTab = function (el) {
        var tabNameAttribute = _this.options.nameAttribute.tab;


        return el.getAttribute(tabNameAttribute);
    };

    this.getTabpanelNameFromTabpanel = function (el) {
        var tabpanelNameAttribute = _this.options.nameAttribute.tabpanel;


        return el.getAttribute(tabpanelNameAttribute);
    };

    this.getTabsByTabpanelName = function (tabpanelName) {
        var tabNameAttribute = _this.options.nameAttribute.tab,
            tabs = _this.nodes.tabs;


        return tabs.filter(function (tab) {
            return tab.getAttribute(tabNameAttribute) === tabpanelName;
        });
    };

    this.getTabpanelsByTabpanelName = function (tabpanelName) {
        var tabpanelNameAttribute = _this.options.nameAttribute.tabpanel,
            tabpanels = _this.nodes.tabpanels;


        return tabpanels.filter(function (tabpanel) {
            return tabpanel.getAttribute(tabpanelNameAttribute) === tabpanelName;
        });
    };

    this.activateTabs = function (newActiveTabs) {
        var activeTabClassName = _this.options.classNames.activeTab;


        newActiveTabs.forEach(function (newActiveTab) {
            return newActiveTab.classList.add(activeTabClassName);
        });
    };

    this.deactivateTabs = function (activeTabs) {
        var activeTabClassName = _this.options.classNames.activeTab;


        activeTabs.forEach(function (activeTab) {
            return activeTab.classList.remove(activeTabClassName);
        });
    };

    this.activateTabpanels = function (newActiveTabpanels) {
        var activateTabpanel = _this.activateTabpanel;


        newActiveTabpanels.forEach(function (tabpanel) {
            return activateTabpanel(tabpanel);
        });
    };

    this.deactivateTabpanels = function (activeTabpanels) {
        var deactivateTabpanel = _this.deactivateTabpanel;


        activeTabpanels.forEach(function (tabpanel) {
            return deactivateTabpanel(tabpanel);
        });
    };

    this.activateTabpanel = function (newActiveTabpanel) {
        var activeTabpanelClassName = _this.options.classNames.activeTabpanel;


        newActiveTabpanel.classList.add(activeTabpanelClassName);
    };

    this.deactivateTabpanel = function (newActiveTabpanel) {
        var activeTabpanelClassName = _this.options.classNames.activeTabpanel;


        newActiveTabpanel.classList.remove(activeTabpanelClassName);
    };

    this.transitionActivateTabpanels = function (newActiveTabpanels) {
        var _options2 = _this.options,
            wrapperSelector = _options2.selectors.wrapper,
            wrapperHeightTransitionClassName = _options2.classNames.wrapperHeightTransition,
            activateTabpanel = _this.activateTabpanel,
            wrappers = _this.nodes.wrappers;
        var getHeight = Tabs.getHeight;


        wrappers.forEach(function (wrapper) {
            var newActiveTabpanel = newActiveTabpanels.find(function (newActiveTabpanel) {
                return newActiveTabpanel.closest(wrapperSelector) === wrapper;
            });

            var transitionend = function transitionend(e) {
                if (e.target !== wrapper) return;

                wrapper.style.height = "";

                wrapper.removeEventListener("transitionend", transitionend);
            };

            wrapper.style.height = getHeight(wrapper) + "px";
            activateTabpanel(newActiveTabpanel);
            wrapper.classList.add(wrapperHeightTransitionClassName);
            wrapper.addEventListener("transitionend", transitionend);
            wrapper.style.height = getHeight(newActiveTabpanel) + "px";
        });
    };

    this.activate = function (tabpanelName) {
        var getTabsByTabpanelName = _this.getTabsByTabpanelName,
            getTabpanelsByTabpanelName = _this.getTabpanelsByTabpanelName,
            activateTabs = _this.activateTabs,
            deactivateTabs = _this.deactivateTabs,
            deactivateTabpanels = _this.deactivateTabpanels,
            transitionActivateTabpanels = _this.transitionActivateTabpanels,
            _active = _this.active,
            activeTabs = _active.tabs,
            activeTabpanels = _active.tabpanels;


        var newActiveTabs = getTabsByTabpanelName(tabpanelName);
        var newActiveTabpanels = getTabpanelsByTabpanelName(tabpanelName);

        activateTabs(newActiveTabs);
        deactivateTabs(activeTabs);
        _this.active.tabs = newActiveTabs;

        transitionActivateTabpanels(newActiveTabpanels);
        deactivateTabpanels(activeTabpanels);
        _this.active.tabpanels = newActiveTabpanels;
    };
};

exports.default = Tabs;
//# sourceMappingURL=ui-tabs.js.map
