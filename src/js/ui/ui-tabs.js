import objectMarge from "lodash/merge";

export default class Tabs {
    options = {
        selectors: {
            parent: ".ui-tabs",
            tab: ".ui-tabs__tab",
            wrapper: ".ui-tabs__wrapper",
            tabpanel: ".ui-tabs__tabpanel",
        },
        classNames: {
            wrapperHeightTransition: "ui-tabs__wrapper_height-transition",
            activeTab: "ui-tabs__tab_active",
            activeTabpanel: "ui-tabs__tabpanel_active"
        },
        nameAttribute: {
            tab: "data-ui-tabs-tabpanel",
            tabpanel: "data-ui-tabs-tabpanel"
        },
        events: {

        }
    };

    nodes = {
        tabs: [],
        wrappers: [],
        tabpanels: []
    };

    active = {};

    constructor (el, options) {
        objectMarge(this.options, options);

        const {
            addEventOnTabs,
            getTabpanelNameFromTab,
            getTabpanelNameFromTabpanel,
            getTabsByTabpanelName,
            getTabpanelsByTabpanelName,
            activateTabs,
            activateTabpanels,
            options: {
                selectors: {
                    parent: parentSelector,
                    tab: tabSelector,
                    wrapper: wrapperSelector,
                    tabpanel: tabpanelSelector,
                },
                classNames: {
                    activeTab: activeTabClassName,
                    activeTabpanel: activeTabpanelClassName
                },
                nameAttribute: {
                    tab: tabNameAttribute,
                    tabpanel: tabpanelNameAttribute
                },
            }
        } = this;

        const parent = this.nodes.parent = el;
        const tabs = this.nodes.tabs;
        const wrappers = this.nodes.wrappers;
        const tabpanels = this.nodes.tabpanels;

        parent.querySelectorAll(tabSelector).forEach(tab => tab.closest(parentSelector) === parent && tabs.push(tab));
        parent.querySelectorAll(wrapperSelector).forEach(wrapper => {
            wrapper.closest(parentSelector) === parent && wrappers.push(wrapper);
            wrapper.querySelectorAll(tabpanelSelector).forEach(tabpanel => tabpanel.closest(wrapperSelector) === wrapper && tabpanels.push(tabpanel));
        });

        addEventOnTabs();

        const activeTabs = this.active.tabs = tabs.filter(tab => tab.matches('.' + activeTabClassName));
        const activeTabpanels = this.active.tabpanels = tabpanels.filter(tabpanel => tabpanel.matches('.' + activeTabpanelClassName));

        if (!activeTabs.length && !activeTabpanels.length) {
            const activeTabpanelName = getTabpanelNameFromTab(tabs[0]);

            const newActiveTabs = this.active.tabs = getTabsByTabpanelName(activeTabpanelName);
            activateTabs(newActiveTabs);

            const newActiveTabpanels = this.active.tabpanels = getTabpanelsByTabpanelName(activeTabpanelName);
            activateTabpanels(newActiveTabpanels);
        } else if (activeTabs.length) {
            const activeTabpanelName = getTabpanelNameFromTabpanel(activeTabs[0]);

            if (!activeTabs.every(activeTab => activeTab.getAttribute(tabNameAttribute) === activeTabpanelName)) {
                tabs.forEach((tab, i) => {
                    if (i < 1)
                        return;

                    if (tab.getAttribute(tabNameAttribute) === activeTabpanelName && !tab.classList.contains(activeTabClassName)) {
                        tab.classList.add(activeTabClassName);
                    } else if (tab.classList.contains(activeTabClassName) && tab.getAttribute(tabNameAttribute) !== activeTabpanelName) {
                        tab.classList.remove(activeTabClassName);
                    }
                });
            }

            const newActiveTabpanels = this.active.tabpanels = getTabpanelsByTabpanelName(activeTabpanelName);
            activateTabpanels(newActiveTabpanels);
        } else if (activeTabpanels.length) {
            const activeTabpanelName = getTabpanelNameFromTabpanel(activeTabpanels[0]);

            if (!activeTabpanels.every(activeTabpanel => activeTabpanel.getAttribute(tabpanelNameAttribute) === activeTabpanelName)) {
                tabpanels.forEach((tabpanel, i) => {
                    if (i < 1)
                        return;

                    if (tabpanel.getAttribute(tabpanelNameAttribute) === activeTabpanelName && !tabpanel.classList.contains(activeTabpanelClassName)) {
                        tabpanel.classList.add(activeTabpanelClassName);
                    } else if (tabpanel.classList.contains(activeTabpanelClassName) && tabpanel.getAttribute(tabpanelNameAttribute) !== activeTabpanelName) {
                        tabpanel.classList.remove(activeTabpanelClassName);
                    }
                });
            }

            const newActiveTabs = this.active.tab = getTabsByTabpanelName(activeTabpanelName);
            activateTabs(newActiveTabs);
        }
    }

    onTabClick = e => {
        e.preventDefault();

        const {
            getTabpanelNameFromTab,
            activate
        } = this;

        activate(getTabpanelNameFromTab(e.currentTarget));
    };

    addEventOnTabs = () => {
        const {
            onTabClick,
            nodes: {
                tabs
            }
        } = this;

        tabs.forEach(tab => tab.addEventListener("click", onTabClick));
    };

    removeEventOnTabs = () => {
        const {
            onTabClick,
            nodes: {
                tabs
            }
        } = this;

        tabs.forEach(tab => tab.removeEventListener("click", onTabClick));
    };

    getTabpanelNameFromTab = (el) => {
        const {
            options: {
                nameAttribute: {
                    tab: tabNameAttribute
                }
            }
        } = this;

        return el.getAttribute(tabNameAttribute);
    };

    getTabpanelNameFromTabpanel = (el) => {
        const {
            options: {
                nameAttribute: {
                    tabpanel: tabpanelNameAttribute
                }
            }
        } = this;

        return el.getAttribute(tabpanelNameAttribute);
    };

    getTabsByTabpanelName = (tabpanelName) => {
        const {
            options: {
                nameAttribute: {
                    tab: tabNameAttribute
                }
            },
            nodes: {
                tabs
            }
        } = this;

        return tabs.filter(tab => tab.getAttribute(tabNameAttribute) === tabpanelName);
    };

    getTabpanelsByTabpanelName = (tabpanelName) => {
        const {
            options: {
                nameAttribute: {
                    tabpanel: tabpanelNameAttribute
                }
            },
                nodes: {
                tabpanels
            }
        } = this;

        return tabpanels.filter(tabpanel => tabpanel.getAttribute(tabpanelNameAttribute) === tabpanelName);
    };

    activateTabs = (newActiveTabs) => {
        const {
            options: {
                classNames: {
                    activeTab: activeTabClassName
                }
            }
        } = this;

        newActiveTabs.forEach(newActiveTab => newActiveTab.classList.add(activeTabClassName));
    };

    deactivateTabs = (activeTabs) => {
        const {
            options: {
                classNames: {
                    activeTab: activeTabClassName
                }
            }
        } = this;

        activeTabs.forEach(activeTab => activeTab.classList.remove(activeTabClassName));
    };

    activateTabpanels = (newActiveTabpanels) => {
        const {
            activateTabpanel
        } = this;

        newActiveTabpanels.forEach(tabpanel => activateTabpanel(tabpanel));
    };

    deactivateTabpanels = (activeTabpanels) => {
        const {
            deactivateTabpanel
        } = this;

        activeTabpanels.forEach(tabpanel => deactivateTabpanel(tabpanel));
    };

    activateTabpanel = (newActiveTabpanel) => {
        const {
            options: {
                classNames: {
                    activeTabpanel: activeTabpanelClassName
                }
            }
        } = this;

        newActiveTabpanel.classList.add(activeTabpanelClassName);
    };

    deactivateTabpanel = (newActiveTabpanel) => {
        const {
            options: {
                classNames: {
                    activeTabpanel: activeTabpanelClassName
                }
            }
        } = this;

        newActiveTabpanel.classList.remove(activeTabpanelClassName);
    };

    transitionActivateTabpanels = (newActiveTabpanels) => {
        const {
            options: {
                selectors: {
                    wrapper: wrapperSelector
                },
                classNames: {
                    wrapperHeightTransition: wrapperHeightTransitionClassName
                }
            },
            activateTabpanel,
            nodes: {
                wrappers
            }
        } = this;

        const {
            getHeight
        } = Tabs;


        wrappers.forEach(wrapper => {
            const newActiveTabpanel = newActiveTabpanels.find(newActiveTabpanel => newActiveTabpanel.closest(wrapperSelector) === wrapper);

            const transitionend = e => {
                if (e.target !== wrapper)
                    return;

                wrapper.style.height = "";

                wrapper.removeEventListener("transitionend", transitionend);
            };

            wrapper.style.height = getHeight(wrapper) + "px";
            activateTabpanel(newActiveTabpanel);
            wrapper.classList.add(wrapperHeightTransitionClassName);
            wrapper.addEventListener("transitionend", transitionend);
            wrapper.style.height = getHeight(newActiveTabpanel) + "px";
        });
    };

    activate = (tabpanelName) => {
        const {
            getTabsByTabpanelName,
            getTabpanelsByTabpanelName,
            activateTabs,
            deactivateTabs,
            deactivateTabpanels,
            transitionActivateTabpanels,

            active: {
                tabs: activeTabs,
                tabpanels: activeTabpanels
            }
        } = this;

        const newActiveTabs = getTabsByTabpanelName(tabpanelName);
        const newActiveTabpanels = getTabpanelsByTabpanelName(tabpanelName);

        activateTabs(newActiveTabs);
        deactivateTabs(activeTabs);
        this.active.tabs = newActiveTabs;

        transitionActivateTabpanels(newActiveTabpanels);
        deactivateTabpanels(activeTabpanels);
        this.active.tabpanels = newActiveTabpanels;
    };

    static getHeight(el) {
        return el.offsetHeight;
    };
}