import gulp from 'gulp';
import sass from 'gulp-sass';
import webpack from "webpack";
import webpackStream from "webpack-stream";
import named from "vinyl-named";
import browserSync from "browser-sync";
import sourcemaps from "gulp-sourcemaps";
import babel from "gulp-babel";
import gulpIf from "gulp-if";
import combiner from "stream-combiner2";
import path from "path";

const tasks = {
    sassCompile: "sass-compile",
    webpack: "webpack",

    watch: "watch",
    browserSync: "browser-sync",
    dev: "dev"
};

gulp.task(tasks.sassCompile, () => gulp.src("./src/stylesheet/**/*.{sass,scss}")
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest("./dist/css"))
);


gulp.task('ui-tabs', () => gulp.src('src/js/ui/ui-tabs.js')
    .pipe(sourcemaps.init())
    .pipe(babel({
        presets: ['env'],
        plugins: ["transform-class-properties"]
    }))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest('dist/js'))
);

gulp.task(tasks.webpack, () => {
    const config = require("./config/webpack.config.js");

    config.watch = true;

    return gulp.src("./src/js/app.js")
        .pipe(named(file => file.relative.substring(0, file.relative.length - file.extname.length)))
        .pipe(webpackStream(config, webpack))
        .pipe(gulp.dest("./dist/js"))
});

gulp.task(tasks.watch, () => {
    gulp.watch("./src/stylesheet/**/*.{sass,scss}", gulp.series(tasks.sassCompile));
    gulp.watch("./src/js/ui/ui-tabs.js", gulp.series("ui-tabs"));
});

gulp.task(tasks.browserSync, () => {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });

    browserSync.watch(["./dist/*.*", "./*.html"]).on('change', browserSync.reload)
});

gulp.task(tasks.dev, gulp.series(
    tasks.sassCompile,
   "ui-tabs",
    gulp.parallel(
        tasks.webpack,
        tasks.watch,
        tasks.browserSync
    )
));
